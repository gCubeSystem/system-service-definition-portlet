package org.gcube.portlets.admin.systemservicedefinition;

import static org.junit.Assert.assertTrue;

import org.gcube.portlets.admin.systemservicedefinition.definition.DefinitionItem;
import org.gcube.portlets.admin.systemservicedefinition.is.InformationSystemUtils;
import org.gcube.portlets.admin.systemservicedefinition.shared.Constants;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ISTest {

	private static Logger logger = LoggerFactory.getLogger(ISTest.class);

	@Ignore
	public void findSSD() {

		try {
			InformationSystemUtils.retrieveSSD(Constants.SYSTEM_SERVICE_CATEGORY, Constants.DEFAULT_SCOPE);
			assertTrue(true);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			assertTrue("Error searching the resource!", false);
		}

	}

	@Ignore
	public void publishSSD() {

		try {
			DefinitionItem definitionItem = new DefinitionItem("sample-client-name", "sample-client-description",
					"sample-client-description", "xxxxx-xxxx-xxx-xxxx");
			InformationSystemUtils.publishSSD(definitionItem, Constants.SYSTEM_SERVICE_CATEGORY,
					Constants.DEFAULT_SCOPE);
			assertTrue(true);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			assertTrue("Error publishing the resource!", false);
		}

	}

	@Ignore
	public void deleteSSD() {

		try {
			DefinitionItem definitionItem = new DefinitionItem("sample-client-name", "sample-client-description",
					"sample-client-description", "xxxxx-xxxx-xxx-xxxx");
			InformationSystemUtils.deleteSSD(definitionItem, Constants.SYSTEM_SERVICE_CATEGORY,
					Constants.DEFAULT_SCOPE);
			assertTrue(true);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			assertTrue("Error deleting the resource!", false);
		}

	}

	
	
}