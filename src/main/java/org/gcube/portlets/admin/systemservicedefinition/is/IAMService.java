package org.gcube.portlets.admin.systemservicedefinition.is;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class IAMService implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String address;

	public IAMService(String address) {
		super();
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "IAMService [address=" + address + "]";
	}
	
	
}
