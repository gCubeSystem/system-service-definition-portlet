package org.gcube.portlets.admin.systemservicedefinition.util;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class UserCredentials implements Serializable {

	private static final long serialVersionUID = -3020343255604840519L;
	private long userId;
	private String username;
	private String currentScope;
	private String accessToken;

	public UserCredentials() {
		super();
	}

	public UserCredentials(long userId, String username, String currentScope, String accessToken) {
		super();
		this.userId = userId;
		this.username = username;
		this.currentScope = currentScope;
		this.accessToken = accessToken;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCurrentScope() {
		return currentScope;
	}

	public void setCurrentScope(String currentScope) {
		this.currentScope = currentScope;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "UserCredentials [userId=" + userId + ", username=" + username + ", currentScope=" + currentScope
				+ ", accessToken=" + accessToken + "]";
	}

}