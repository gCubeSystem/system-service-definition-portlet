package org.gcube.portlets.admin.systemservicedefinition.event;

import org.gcube.portal.event.publisher.lr62.AbstractLR62EventPublisher;
import org.gcube.portal.event.publisher.lr62.PortalEvent;
import org.gcube.portlets.admin.systemservicedefinition.definition.DefinitionItem;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class CreateSSDEvent extends PortalEvent {

	private static final long serialVersionUID = 1499288552188273747L;

	public class CreateSSDEventPublisher extends AbstractLR62EventPublisher {
		public CreateSSDEventPublisher() {
			super();
		}
	}

	public CreateSSDEventPublisher publisher;

	public static final String NAME = "create_system_service";
	public static final String CLIENT_ID_ENTRY = "client_id";
	public static final String DESCRIPTION_ENTRY = "description";
	public static final String CLIENT_SECRET_ENTRY = "client_sercret";

	public CreateSSDEvent(DefinitionItem definitionItem) {
		super(NAME);
		publisher = new CreateSSDEventPublisher();
		setClientId(definitionItem.getClientId());
		setDescription(definitionItem.getDescription());
		setClientSecret(definitionItem.getSecret());
	}

	public void setClientId(String clientId) {
		set(CLIENT_ID_ENTRY, clientId);
	}

	public String getClientId() {
		return (String) get(CLIENT_ID_ENTRY);
	}

	public void setDescription(String description) {
		set(DESCRIPTION_ENTRY, description);
	}

	public String getDescription() {
		return (String) get(DESCRIPTION_ENTRY);
	}

	public void setClientSecret(String clientSecret) {
		set(CLIENT_SECRET_ENTRY, clientSecret);
	}

	public String getClientSecret() {
		return (String) get(CLIENT_SECRET_ENTRY);
	}

}
