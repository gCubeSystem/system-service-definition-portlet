package org.gcube.portlets.admin.systemservicedefinition;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.portlets.admin.systemservicedefinition.util.UserCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class SessionUtil {

	private static Logger logger = LoggerFactory.getLogger(SessionUtil.class);

	
	public static UserCredentials getUserCredentials(RenderRequest renderRequest) throws Exception {
		User theUser = PortalUtil.getUser(renderRequest);
		long userId=theUser.getUserId();
		String username=theUser.getScreenName();
		long groupId = PortalUtil.getScopeGroupId(renderRequest);
		PortalContext pContext = PortalContext.getConfiguration(); 
		String currentScope=pContext.getCurrentScope(""+groupId);
		
		String accessToken = pContext.getCurrentUserToken(currentScope, username);
		
		/*
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
		JWTToken umaToken = OIDCUmaUtil.getUMAToken(httpRequest, username, currentScope);  //context non encodato, e.g. /gcube/devsec
		//oppure OIDCUmaUtil.getUMAToken(httprequest, userId (long), context);
		AccessTokenProvider.instance.set(JWTTokenUtil.getAccessTokenString(umaToken));
		String accessToken = AccessTokenProvider.instance.get();
		*/
		
		UserCredentials serviceCredentials = new UserCredentials(userId, username, currentScope,accessToken);
		logger.debug(serviceCredentials.toString());
		return serviceCredentials;
	}
	
	public static UserCredentials getUserCredentials(ActionRequest actionRequest) throws Exception {
		User theUser = PortalUtil.getUser(actionRequest);
		long userId=theUser.getUserId();
		String username=theUser.getScreenName();
		long groupId = PortalUtil.getScopeGroupId(actionRequest);
		PortalContext pContext = PortalContext.getConfiguration(); 
		String currentScope=pContext.getCurrentScope(""+groupId);
		
		String accessToken = pContext.getCurrentUserToken(currentScope, username);
		
		/*
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(actionRequest));
		JWTToken umaToken = OIDCUmaUtil.getUMAToken(httpRequest, username, currentScope);  //context non encodato, e.g. /gcube/devsec
		//oppure OIDCUmaUtil.getUMAToken(httprequest, userId (long), context);
		AccessTokenProvider.instance.set(JWTTokenUtil.getAccessTokenString(umaToken));
		String accessToken = AccessTokenProvider.instance.get();
		*/
		
		UserCredentials serviceCredentials = new UserCredentials(userId, username, currentScope, accessToken);
		logger.debug(serviceCredentials.toString());
		return serviceCredentials;
	}

	public static UserCredentials getUserCredentials(ResourceRequest resourceRequest) throws Exception{

		User theUser= PortalUtil.getUser(resourceRequest);
		long userId=theUser.getUserId();
		String username=theUser.getScreenName();
		long groupId = PortalUtil.getScopeGroupId(resourceRequest);
		PortalContext pContext = PortalContext.getConfiguration();
		String currentScope = pContext.getCurrentScope("" + groupId);
		
		String accessToken = pContext.getCurrentUserToken(currentScope, username);
		
		/*
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(resourceRequest));
		JWTToken umaToken = OIDCUmaUtil.getUMAToken(httpRequest, username, currentScope);  //context non encodato, e.g. /gcube/devsec
		//oppure OIDCUmaUtil.getUMAToken(httprequest, userId (long), context);
		AccessTokenProvider.instance.set(JWTTokenUtil.getAccessTokenString(umaToken));
		String accessToken = AccessTokenProvider.instance.get();
		*/
		
		UserCredentials serviceCredentials = new UserCredentials(userId, username, currentScope, accessToken);
		logger.debug(serviceCredentials.toString());
		return serviceCredentials;
	}

}
