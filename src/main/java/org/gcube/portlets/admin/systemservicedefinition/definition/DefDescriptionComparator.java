package org.gcube.portlets.admin.systemservicedefinition.definition;

import java.util.Comparator;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class DefDescriptionComparator implements Comparator<DefinitionItem>{

	@Override
	public int compare(DefinitionItem d1, DefinitionItem d2) {
		return d1.getClientId().compareTo(d2.getClientId());
	}

}