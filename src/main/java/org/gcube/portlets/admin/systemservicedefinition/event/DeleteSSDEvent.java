package org.gcube.portlets.admin.systemservicedefinition.event;

import org.gcube.portal.event.publisher.lr62.AbstractLR62EventPublisher;
import org.gcube.portal.event.publisher.lr62.PortalEvent;
import org.gcube.portlets.admin.systemservicedefinition.definition.DefinitionItem;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class DeleteSSDEvent extends PortalEvent {

	private static final long serialVersionUID = 1499288552188273747L;

	public class DeleteSSDEventPublisher extends AbstractLR62EventPublisher {
		public DeleteSSDEventPublisher() {
			super();
		}
	}

	public DeleteSSDEventPublisher publisher;

	public static final String NAME = "delete_system_service";
	public static final String CLIENT_ID_ENTRY = "client_id";

	public DeleteSSDEvent(DefinitionItem definitionItem) {
		super(NAME);
		publisher = new DeleteSSDEventPublisher();
		setClientId(definitionItem.getClientId());
	}

	public void setClientId(String clientId) {
		set(CLIENT_ID_ENTRY, clientId);
	}

	public String getClientId() {
		return (String) get(CLIENT_ID_ENTRY);
	}

}
