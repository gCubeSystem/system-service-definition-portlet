<%@include file="init.jsp"%>

<portlet:renderURL var="ssdDialogCreateContent"
	windowState="<%=LiferayWindowState.EXCLUSIVE.toString()%>">
	<portlet:param name="mvcPath"
		value="/html/systemservicedefinition/ssd_dialog_create_content.jsp" />
	<portlet:param name="message" value="Hello welcome" />
</portlet:renderURL>



<div>
	<aui:button name="ssd_dialog_create" id="ssd_dialog_create"
		value="Create">
	</aui:button>
</div>
<aui:script>
AUI().use('aui-base',
	'aui-io-plugin-deprecated',
	'liferay-util-window', 
	function(A) {
		A.one('#<portlet:namespace />ssd_dialog_create').on('click', function(event){ 
			var popUpWindow=Liferay.Util.Window.getWindow(
						{
							dialog: {
								centered: true,
								constrain2view: true,
								//cssClass: 'yourCSSclassName',
								modal: true,
								resizable: false,
								width: 475
							}
						}
					).plug(
						A.Plugin.IO,
						{
							autoLoad: false
						}).render();
			popUpWindow.show();
			popUpWindow.titleNode.html("System Service Definition Dialog");
			popUpWindow.io.set('uri','<%=ssdDialogCreateContent%>');
			popUpWindow.io.start();
		});
	});
</aui:script>
