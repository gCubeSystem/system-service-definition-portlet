This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "system-services-definition-portlet"


## [v1.0.0] - 2021-10-20

### Fixes

- First Release [#21362]